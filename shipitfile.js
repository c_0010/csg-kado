module.exports = shipit => {
  require('shipit-deploy')(shipit);
  require('shipit-npm')(shipit);

  shipit.initConfig({
    default: {
      ignores: ['.git', 'node_modules'],
      keepReleases: 2,
      deployTo: '/var/www/kado',
      repositoryUrl: 'git@gitlab.lirmi.com:cristian/kado.git',
      npm: {
        remote: true,
        installFlags: ['--production'],
      },
    },
    staging: {
      servers: [
        {
          host: '142.93.78.249',
          user: 'ubuntu',
        },
      ],
    },
  });

  shipit.on('deployed', async () => {
    shipit.start('dotenv');
    shipit.start('dist');
    shipit.start('reload');
    await shipit.remote(`chmod -R 755 ${shipit.releasePath}`);
  });

  shipit.task('reload', () => {
    shipit.remote(`pm2 restart ${shipit.currentPath}/ecosystem.json`);
  });

  shipit.task('dotenv', async () => {
    shipit.copyToRemote('.env.example', `${shipit.releasePath}/.env`);
  });

  shipit.task('dist', async () => {
    await shipit.local('npm run prod');
    shipit.copyToRemote('dist', shipit.releasePath);
  });
};
