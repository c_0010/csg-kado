module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": ["plugin:prettier/recommended", "plugin:vue/recommended"],
    "parserOptions": {
        "parser": "babel-eslint",
        "ecmaVersion": 2017,
        "sourceType": "module"
    },
    "plugins": ["prettier", "vue"],
    "rules": {
        "prettier/prettier": ["error", {
            "printWidth": 120,
            "tabWidth": 2,
            "useTabs": false,
            "semi": true,
            "singleQuote": true,
            "trailingComma": "es5"
        }]
    }
};