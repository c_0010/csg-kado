const Model = require('./BaseModel');

class EtapaModel extends Model {
  static get tableName() {
    return 'etapas';
  }

  static get relationMappings() {
    return {
      siguiente: {
        relation: Model.HasOneRelation,
        modelClass: this,
        join: {
          from: 'etapas.siguiente_etapa_id',
          to: 'etapas.id',
        },
      },
    };
  }
}

module.exports = EtapaModel;
