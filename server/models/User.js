const Model = require('./BaseModel');

class UserModel extends Model {
  static tableName() {
    return 'users';
  }
}

module.exports = UserModel;
