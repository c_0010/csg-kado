const Model = require('./BaseModel');

class TallaModel extends Model {
  static tableName() {
    return 'tallas';
  }
}

module.exports = TallaModel;
