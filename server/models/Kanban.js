const Model = require('./BaseModel');
const User = require('./User');
const Talla = require('./Talla');
const Etapa = require('./Etapa');

class KanbanModel extends Model {
  static get tableName() {
    return 'kanbans';
  }

  static get relationMappings() {
    return {
      responsables: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        join: {
          from: 'kanbans.id',
          through: {
            from: 'kanban_user.kanban_id',
            to: 'kanban_user.user_id',
          },
          to: 'users.id',
        },
      },
      talla: {
        relation: Model.BelongsToOneRelation,
        modelClass: Talla,
        join: {
          from: 'kanbans.talla_id',
          to: 'tallas.id',
        },
      },
      etapa: {
        relation: Model.BelongsToOneRelation,
        modelClass: Etapa,
        join: {
          from: 'kanbans.etapa_id',
          to: 'etapas.id',
        },
      },
    };
  }

  $beforeInsert() {
    this.created_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

module.exports = KanbanModel;
