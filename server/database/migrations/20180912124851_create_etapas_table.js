exports.up = function(knex, Promise) {
  return knex.schema.createTable('etapas', table => {
    table.increments();
    table.string('nombre');
    table
      .integer('siguiente_etapa_id')
      .unsigned()
      .nullable();
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('etapas');
};
