exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanban_user', table => {
    table
      .integer('user_id')
      .unsigned()
      .index();
    table
      .integer('kanban_id')
      .unsigned()
      .index();
    table.primary(['user_id', 'kanban_id']);

    table
      .foreign('user_id')
      .references('users.id')
      .onDelete('cascade');
    table
      .foreign('kanban_id')
      .references('kanbans.id')
      .onDelete('cascade');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanban_user');
};
