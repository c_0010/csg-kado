exports.up = function(knex, Promise) {
  return knex.schema.createTable('tallas', table => {
    table.increments();
    table.string('descripcion');
    table.string('etiqueta');
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('tallas');
};
