exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanbans', table => {
    table.increments();
    table
      .integer('autor_id')
      .unsigned()
      .notNullable()
      .index();
    table
      .integer('etapa_id')
      .unsigned()
      .index();
    table
      .integer('talla_id')
      .unsigned()
      .index();
    table.text('descripcion');
    table.jsonb('hitos').default(JSON.stringify({ develop: null, feedback: null, production: null }));
    table
      .float('lead_time')
      .unsigned()
      .default(0);
    table.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanbans');
};
