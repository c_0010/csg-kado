exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('tallas')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('tallas').insert([
        { id: 1, descripcion: 'XS', etiqueta: 'XS' },
        { id: 2, descripcion: 'S', etiqueta: 'S' },
        { id: 3, descripcion: 'M', etiqueta: 'M' },
        { id: 4, descripcion: 'L', etiqueta: 'L' },
        { id: 5, descripcion: 'XL', etiqueta: 'XL' },
      ]);
    });
};
