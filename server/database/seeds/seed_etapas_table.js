exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('etapas')
    .del()
    .then(function() {
      // Inserts seed entries
      return knex('etapas').insert([
        { id: 1, nombre: 'develop', siguiente_etapa_id: 2 },
        { id: 2, nombre: 'feedback', siguiente_etapa_id: 3 },
        { id: 3, nombre: 'production', siguiente_etapa_id: null },
      ]);
    });
};
