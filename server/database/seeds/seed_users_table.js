exports.seed = function(knex, Promise) {
  return knex('users')
    .del()
    .then(function() {
      return knex('users').insert([
        {
          nombre: 'Cristian',
          email: 'cristian@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
        {
          nombre: 'Carlos',
          email: 'carlos.h@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
        {
          nombre: 'Felipe',
          email: 'felipe@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
        {
          nombre: 'Ricardo',
          email: 'ricardo@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
        {
          nombre: 'Rodrigo',
          email: 'ricardo@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
        {
          nombre: 'Matias',
          email: 'matias@lirmi.com',
          password: '$2b$10$pl5rSFgZHLJNDvpRBlo48uaDHJMp.g9kIgcbXgR6uXjK7v20YPFne',
        },
      ]);
    });
};
