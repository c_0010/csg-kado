const Etapa = require('../models/Etapa');

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/etapas',
    schema: {
      response: {
        200: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              nombre: { type: 'string' },
              siguiente: {},
            },
          },
        },
      },
    },
    handler: async (request, reply) => {
      const data = await Etapa.query().eager('siguiente');
      return data;
    },
  });

  next();
};
