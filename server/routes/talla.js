const db = require('../knex');

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/tallas',
    scheme: {
      response: {
        200: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'number' },
              nombre: { type: 'string' },
            },
          },
        },
      },
    },
    handler: async (request, reply) => {
      const data = await db('tallas');
      return data;
    },
  });

  next();
};
