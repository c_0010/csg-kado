const db = require('../knex');

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/tiempos-promedios',
    scheme: {
      response: {
        200: {
          type: 'object',
          properties: {
            promedio: 'number',
          },
        },
      },
    },
    handler: async (request, reply) => {
      const etapaId = request.query.etapa_id;
      const promedio = await db('kanbans')
        .where({ etapa_id: etapaId })
        .avg({ promedio: 'lead_time' })
        .first();
      return promedio;
    },
  });

  next();
};
