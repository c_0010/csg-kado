const db = require('../knex');

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/users',
    handler: async (request, reply) => {
      const data = await db('users');

      return data;
    },
  });

  next();
};
