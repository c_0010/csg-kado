const Kanban = require('../models/Kanban');
const db = require('../knex');
const eachDay = require('date-fns/each_day');
const isWeekend = require('date-fns/is_weekend');

const asociarResponsables = async (kanban, ids) => {
  await db('kanban_user')
    .where({ kanban_id: kanban.id })
    .del();

  const query = ids.map(id => {
    return {
      kanban_id: kanban.id,
      user_id: id,
    };
  });

  return db('kanban_user').insert(query);
};

const calcularLeadTime = (etapaId, hitos) => {
  if (etapaId !== 3) {
    return 0;
  }
  let dias = 0;
  eachDay(hitos.develop, hitos.production).forEach(dia => {
    if (!isWeekend(dia)) {
      ++dias;
    }
  });
  return dias;
};

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/kanbans',
    handler: async (request, reply) => {
      const {
        query: { etapa_id },
      } = request;

      let data = [];
      data = await Kanban.query()
        .where({ etapa_id })
        .eager('[responsables,talla]');
      reply.send(data);
    },
  });

  fastify.route({
    method: 'POST',
    url: '/kanbans',
    handler: async (request, reply) => {
      const { descripcion, etapa_id, talla_id, autor_id, responsables_ids, hitos } = request.body;
      const kanban = await Kanban.query().insert({ hitos, descripcion, etapa_id, talla_id, autor_id });
      if (responsables_ids && Array.isArray(responsables_ids)) {
        await asociarResponsables(kanban, responsables_ids);
      }

      return kanban;
    },
  });

  fastify.route({
    method: 'PATCH',
    url: '/kanbans/:id',
    handler: async (request, reply) => {
      const { id } = request.params;
      const { descripcion, etapa_id, talla_id, responsables_ids, hitos } = request.body;
      let lead_time = calcularLeadTime(etapa_id, hitos);
      const kanban = await Kanban.query()
        .where({ id })
        .patch({ descripcion, etapa_id, talla_id, hitos, lead_time });

      if (responsables_ids && Array.isArray(responsables_ids)) {
        await asociarResponsables({ id }, responsables_ids);
      }

      return kanban;
    },
  });

  fastify.route({
    method: 'GET',
    url: '/kanbans/:id',
    scheme: {
      response: {
        200: {
          type: 'object',
          properties: {
            id: 'number',
            autor_id: 'number',
            etapa_id: 'number',
            talla_id: 'number',
            descripcion: 'string',
            hitos: 'object',
            lead_time: 'number',
            responsables: 'array',
          },
        },
      },
    },
    handler: async (request, reply) => {
      const { id } = request.params;
      const { incluye } = request.query;
      const kanban = await Kanban.query()
        .eager(incluye)
        .findById(id);

      return kanban;
    },
  });

  next();
};
