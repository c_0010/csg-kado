module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'GET',
    url: '/me',
    beforeHandler: [fastify.authenticate],
    handler: (request, reply) => {
      const { user } = request;
      reply.send(user);
    },
  });

  next();
};
