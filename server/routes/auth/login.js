const db = require('../../knex');

module.exports = (fastify, opts, next) => {
  fastify.route({
    method: 'POST',
    url: '/autentificar',
    handler: async (request, reply) => {
      const { email, password } = request.body;
      fastify.log.info(request.body);

      const user = await db('users')
        .where({ email })
        .first('id', 'email', 'password', 'api_token');

      if (!user) {
        reply.code(422).send({ mensaje: 'Credenciales invalidas' });
      }

      const token = fastify.jwt.sign({ id: user.id, email: user.email });
      reply.send(token);
    },
  });
  next();
};
