'use strict';

const fastify = require('fastify')({
  logger: {
    level: 'info',
  },
});

fastify.register(require('./auth'));

fastify.register(require('./routes/auth/login'), { prefix: '/api' });
fastify.register(require('./routes/auth/me'), { prefix: '/api' });
fastify.register(require('./routes/etapa'), { prefix: '/api' });
fastify.register(require('./routes/talla'), { prefix: '/api' });
fastify.register(require('./routes/user'), { prefix: '/api' });
fastify.register(require('./routes/kanban'), { prefix: '/api' });
fastify.register(require('./routes/tiempoPromedio'), { prefix: '/api' });

const start = async () => {
  await fastify.listen(8000);
};

start();
