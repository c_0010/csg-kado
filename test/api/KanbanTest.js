const { expect } = require('code');
const Lab = require('lab');
const lab = (exports.lab = Lab.script());

const describe = lab.describe;
const it = lab.it;
const after = lab.after;

const Server = require('../../server/server');

describe('Rest api test - kanbans', () => {
  it('se puede obtener los kanbans', async () => {
    const response = await Server.inject({
      method: 'GET',
      url: '/api/kanbans',
    });
    expect(response.statusCode).to.equal(200);
  });
});
