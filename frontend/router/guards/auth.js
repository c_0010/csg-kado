import store from '../../store';
export default () => {
  if (!store.state.auth) {
    return store.dispatch('traerAuth');
  }

  return Promise.resolve(store.state.auth);
};
