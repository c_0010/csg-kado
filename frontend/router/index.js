import store from '../store';
import VueRouter from 'vue-router';
import Vue from 'vue';
import Auth from './guards/auth';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: { name: 'home.develop' },
  },
  {
    name: 'login',
    path: '/login',
    component: () => import('../pages/auth/Login.vue'),
    beforeEnter: (to, from, next) => {
      if (store.state.auth) {
        next({ name: 'home.develop' });
      }

      next();
    },
  },
  {
    name: 'home.develop',
    path: '/develop',
    meta: {
      auth: true,
    },
    component: () => import('../pages/Develop.vue'),
  },
  {
    name: 'home.feedback',
    path: '/feedback',
    meta: {
      auth: true,
    },
    component: () => import('../pages/Feedback.vue'),
  },
  {
    name: 'home.production',
    path: '/production',
    meta: {
      auth: true,
    },
    component: () => import('../pages/Production.vue'),
  },
  {
    path: '/kanban/crear',
    name: 'kanban.crear',
    meta: {
      auth: true,
    },
    component: () => import('../pages/kanban/Crear.vue'),
  },
  {
    path: '/kanbans/:id',
    name: 'kanban.editar',
    meta: {
      auth: true,
    },
    props: true,
    component: () => import('../pages/kanban/Editar.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
  linkExactActiveClass: 'active',
});

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    return Auth()
      .then(() => next())
      .catch(() => next({ name: 'login' }));
  }

  next();
});

export default router;
