import Vue from 'vue';
import './sass/app';
import store from './store';

import router from './router';
import App from './pages/App';

store.dispatch('traerEstaticos');
const app = new Vue({
  store,
  router,
  el: '#app',
  render: h => h(App),
});
