import Vue from 'vue';
import Vuex from 'vuex';
import Axios from '../axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    auth: null,
    etapas: [],
    tallas: [],
    users: [],
  },
  actions: {
    traerEstaticos({ commit }) {
      Axios.get('etapas').then(({ data }) => commit('SET_ETAPAS', data));
      Axios.get('tallas').then(({ data }) => commit('SET_TALLAS', data));
      Axios.get('users').then(({ data }) => commit('SET_USERS', data));
    },

    traerAuth({ commit }) {
      return Axios.get('me').then(({ data }) => commit('SET_AUTH', data));
    },
  },

  mutations: {
    SET_ETAPAS(state, etapas) {
      state.etapas = etapas;
    },

    SET_TALLAS(state, tallas) {
      state.tallas = tallas;
    },

    SET_USERS(state, users) {
      state.users = users;
    },

    SET_AUTH(state, user) {
      state.auth = user;
    },
  },
});
